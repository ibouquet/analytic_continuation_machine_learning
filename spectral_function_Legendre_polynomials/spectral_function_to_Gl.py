#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 12 14:25:33 2020

@author: ilanbouquet

Compute the Gl coefficients from the spectral functions using the TRIQS library saved as ./Gl/Gl_xxx.dat
if noise ==1 noisy Gl are computed saved as ./Gl_noise_xxx/Gl_xxx.dat
Inputs = beta lmax noise
"""
import time,sys,os,glob
import numpy as np
from pytriqs.gf import GfLegendre
from triqs_maxent import maxent_util
from multiprocessing import Pool
import shutil



def spectral_function_to_Gl(omega,A_omega_name):
    
    """ Compute the G_tau from the A_omega
        Generate the Gl from the G_tau """
    
    
    A_omega=np.loadtxt(A_omega_name)
    
    g_tau = maxent_util.get_G_tau_from_A_w(A_w=A_omega, w_points=omega,beta=beta, np_tau=np_tau) # Returns GfImtime
    G_tau=g_tau.data[:, 0, 0].real
    gf_legendre = GfLegendre(indices = g_tau.indices, beta = beta, n_points = lmax) # Initialisation of the Gf_legendre 
    gf_legendre.set_from_imtime(g_tau) # Give to Gf_legendre the values of G_tau as input to compute the Gl
    Gl=gf_legendre.data[:, 0, 0].real
    index_Gl=A_omega_name.split('.dat')[0].split('_')[-1]
    Gl_name='Gl/Gl_'+index_Gl+'.dat'
    np.savetxt(Gl_name,Gl,fmt='%1.5f')  
    
    return Gl_name

def create_directory (path):
    dirpath=str(path)
    if os.path.isdir(dirpath):
        shutil.rmtree(dirpath,ignore_errors=True)
    os.mkdir(dirpath)
    return
    
def noisy_Gl(Gl_name,noise_level):
    
    """ Add different noise level to the Gls """
    
    Gl=np.loadtxt(Gl_name)
    mu=Gl
    sigma=noise_level*np.ones(len(Gl))
    Gl_noise=np.random.normal(mu, sigma, len(Gl))
    
    level=str(noise_level).split('.')[-1]
    index=Gl_name.split('.dat')[0].split('_')[-1]
    np.savetxt('./Gl_noise_'+level+'/'+'Gl_'+index+'.dat',Gl_noise,fmt='%1.5f')
    return


def main(A_omega_name):
    
    omega=np.loadtxt('omega.out')
    Gl_name=spectral_function_to_Gl(omega,A_omega_name)
    
    if noise==1:
        for sigma in noise_levels:
            noisy_Gl(Gl_name,sigma)
            
    np.savetxt('tau_mesh.out',tau_mesh,fmt='%1.5f')
    
    
if __name__ == "__main__":       
    
    start_time = time.time()
    
    beta = float(sys.argv[1])
    lmax = int(sys.argv[2])
    noise=int(sys.argv[3])
    
    np_tau=20001
    tau_mesh = np.linspace(0,beta,np_tau)
    A_omega_list=glob.glob('./A_omega/*dat')
    noise_levels=np.array([0.0001, 0.001, 0.01])    
    
    
    create_directory('./Gl')
    if noise ==1:
        for sigma in noise_levels:
            level=str(sigma).split('.')[-1]
            create_directory('./Gl_noise_'+level)

    pool = Pool(processes=40)
    pool.map(main,A_omega_list)
    
    total_time = time.time() - start_time
 
    print('Legendre polynomials generation took {:.0f} s.'.format(total_time))
    print('beta=',beta,'lmax=',lmax)
    
    
