#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 17:44:39 2020

Generate the all data for the training
Inputs = repetitions beta lmax noise

@author: ilanbouquet
"""

import os,sys
python='python'

def spectral_function (repetitions):
    os.system(python+' '+'spectral_function.py'+' '+repetitions)
    return
    
def A_omega_to_Gl (beta,lmax,noise):
    os.system(python+' '+'spectral_function_to_Gl.py'+' '+beta+ ' '+lmax+' '+noise)
    return

def main():
    
    spectral_function (repetitions)
    A_omega_to_Gl (beta,lmax,noise)
    
if __name__ == "__main__":       
    repetitions=sys.argv[1]
    beta = sys.argv[2]
    lmax = sys.argv[3]
    noise=sys.argv[4]
    
    main()
