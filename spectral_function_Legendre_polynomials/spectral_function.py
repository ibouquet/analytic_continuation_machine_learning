#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 15:25:54 2020

Generation of n spectral functions applying regularisation criteria
Spectral functions saved as /A_omega/A_omega_xxx.dat

@author: ilanbouquet
"""
import matplotlib
matplotlib.use('Agg')
import numpy as np
import random
import matplotlib.pyplot as plt 
from scipy.integrate import simps
import time,sys,os
import shutil


########################################################################################################
# Some functions #
def energy_windows (w,w_range,center_width, step, wr_max):
    
    """ Return the frequencies wr of the R random gaussians center 
        w_center: center regions where the pics are generated
        wr_span: span where the wr are chosen 
        wr_offcenter: region outside the center region """
    
    R=random.randint(4,8) # initial number of center
    
    w_center=w[np.argmax(w>=-center_width):np.argmax(w>=center_width)+1]
    wr_span=w[np.argmax(w>=-wr_max):np.argmax(w>=wr_max)+1]  # take values in the range [-wr_max;wr_max]
    wr_offcenter=np.setxor1d(wr_span,w_center) # intersection between w_center and wr_span 
    wr=np.array(random.sample(wr_span,k=R))
    
    return wr, w_center, wr_span, wr_offcenter 

def arrange_number_of_pics_center (wr,center_width, max_pics,wr_offcenter):
    
    """ Check the number of pics in the center region 
        if nb_center_pics>max_pics move wr to off center region"""
    
    nb_pics=sum(abs(wr)<center_width)
    condition_pics=nb_pics>max_pics
    if condition_pics:
        indices=np.array(np.where(abs(wr)<center_width))
        wr[indices[0][max_pics:]]=np.array(random.sample(wr_offcenter,k=nb_pics-max_pics))
    return wr


def generation_of_sigmar (FWHM_min, FWHM_max, FWHM_threshold, wr, center_width, tolerance_of_center):
    
    """ Generate the gaussian parameters: sigmar, wr, ar and apply the conditions: 
        Condition 1: no center_pics in the near center region
        Condition 2: width of the gaussian < threshold """
    
    # Generation of random widths
    sigma_max=FWHM_max/2*np.sqrt(2*np.log(2))
    sigma_threshold=FWHM_threshold/2*np.sqrt(2*np.log(2))
    minimum_width_ratio=FWHM_min/FWHM_max
    
    sigmar=np.array([])
    for i in range (0,len(wr)):
        n=np.array(random.sample(np.arange(minimum_width_ratio,1.,0.005),k=1))*sigma_max
        sigmar=np.append(sigmar,n)

    #Application of the conditions
    condition1=(abs(wr)>center_width+tolerance_of_center)
    condition2=(sigmar<sigma_threshold)
    condition=condition1*condition2
    
    if  any(condition): # There is narrow pics outside or near the center region 
        indices=np.array(np.where(condition==True))
        wr=np.delete(wr,indices)             
        sigmar=np.delete(sigmar,indices)
        
    ar=1/(sigmar*np.sqrt(2*np.pi)) # Definition of Gausiian's weight
    
    return sigmar, wr, ar  

def A_of_w (wr,sigmar,ar,w):
    
    """ Spectral function is given by summing different gaussians with different centers """
    
    A=np.zeros((len(wr),len(w)))

    for i in range (0,len(wr)):
        A[i,:]=ar[i]*np.exp(-(w-wr[i])**2/sigmar[i]**2)
    
    A_tot=np.sum(A,axis=0) 
    area_underA=simps(A_tot,w)
    A_norm=A_tot/area_underA
    
    return A_norm

def smoothness_criteria_and_ratio (A,w,w_center,center_width,delta,ratio_max):
    
    """ Smoothness criteria = area_underA_center<delta """
    # Smoothness criteria
    Adx2=(w[1]-w[0])**2
    Ady2=np.diff(A,2)
    Ady2dx2=abs(Ady2/Adx2) 
    c_init=np.argmax(w>=-center_width)
    c_end=np.argmax(w>=center_width)+1
    area_underA_center=simps(Ady2dx2[c_init:c_end-2], w_center[:-2])
    under_delta=area_underA_center<delta
    
    #Ratio criteria
    A_center_max=A[c_init:c_end].max()
    A_offcenter_max=np.concatenate((A[0:c_init],A[c_end:-1])).max()
    ratio=abs(A_center_max/A_offcenter_max)
    under_ratio=ratio<ratio_max
 
    return under_delta, under_ratio

def spectral_function(omega):
    
    """ Return A and omega after regularisation criteria """
    
    # Definition of the wr centers
    wr, w_center, wr_span, wr_offcenter = energy_windows (w,w_range,center_width, step, wr_max)

    #Initial value of smoothness criteria_and_ratio set to False
    under_delta, under_ratio = False, False
    k=0
    
    while under_delta*under_ratio==False and k<1000:
    
        wr=arrange_number_of_pics_center(wr,center_width, max_pics_center,wr_offcenter)
        sigmar,wr, ar = generation_of_sigmar (FWHM_min, FWHM_max, FWHM_threshold, wr, center_width, tolerance_of_center)
        A = A_of_w (wr,sigmar,ar,w)
        under_delta, under_ratio = smoothness_criteria_and_ratio (A,w,w_center,center_width,delta,ratio_max)
        k+=1
        
    return A,w

def create_directory (path):
    dirpath=str(path)
    if os.path.isdir(dirpath):
        shutil.rmtree(dirpath,ignore_errors=True)
    os.mkdir(dirpath)

##################################################################################################
# Some constants
w_range=np.array([-1,1])
step=0.01
w=np.arange(w_range[0],w_range[1],step) 
center_width=0.2       # from -2 to 2 
wr_max=0.6
max_pics_center=3
FWHM_min=0.1
FWHM_max=0.5
FWHM_threshold=0.2
tolerance_of_center=0.02
delta=1e6
ratio_max=11


###################################################################################################

def main():
    start_time = time.time()
    print('Starting spectral_function A_omega')
    array_of_A_w=np.zeros((repetitions,len(w)))
    for i in range(repetitions):
         array_of_A_w[i][:],omega=spectral_function(w)
    
    create_directory ('./A_omega')
    np.savetxt('omega.out',omega,fmt='%1.5f')
    for j in range(repetitions):
        np.savetxt('A_omega/A_omega_'+str(j+1)+'.dat',array_of_A_w[j][:],fmt='%1.5f')
      

    total_time = time.time() - start_time
    print('-'*50 + '\nDONE')
    print('A_of_omega generation took {:.0f} s.'.format(total_time))

if __name__ == "__main__":       
    repetitions= int(sys.argv[1])    
    main()


#A,omega=spectral_function()
#plt.plot(omega,A)
#print(A[0],A[-1])




 
 
    






