#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 10:57:32 2020

@author: ilanbouquet
"""


import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense,GaussianNoise
import os, time
import itertools
from multiprocessing import Pool
import glob
import tensorflow as tf


def create_directory (path):
    dirname=str(path)
    if ~os.path.isdir(dirname):
        os.makedirs(dirname,exist_ok=True)
    return 

def training(nodes_batch_epochs_list):
    

    # Input Output shape
    input_shape=Gl.shape[1]
    output_shape=A_omega.shape[1]

    nodes=nodes_batch_epochs_list[0]
    batch=nodes_batch_epochs_list[1]
    epochs=nodes_batch_epochs_list[2]
            
    model = Sequential([
        Dense(nodes, activation='sigmoid', input_shape=(input_shape,)),
        Dense(nodes, activation='sigmoid'),
        GaussianNoise(stddev),
        Dense(nodes, activation='sigmoid'),
        GaussianNoise(stddev),
        Dense(nodes, activation='sigmoid'),
        GaussianNoise(stddev),
        Dense(output_shape, activation='sigmoid'),
        ])

    model.compile(optimizer='adam',loss='mean_squared_error',metrics=['MeanSquaredError'])

    model.fit(Gl_train,A_omega_train,epochs=epochs,batch_size=batch)

    model.save('./models/sigmoid_robust')
    
    model_name='sigmoid_robust'
    path_directory='../predictions/'+model_name
    
    predictions=model.predict(Gl_test)
    create_directory (path_directory)
    np.savetxt(path_directory+'/predictions.dat',predictions)
    
    np.savetxt('../predictions/A_omega_test.dat',A_omega_test)
    np.savetxt('../predictions/omega.out',omega)
    
omega=np.loadtxt('../spectral_function_Legendre_polynomials/omega.out')
A_omega_names=glob.glob('../spectral_function_Legendre_polynomials/A_omega/*dat')
A_omega_names.sort()
Gl_names=glob.glob('../spectral_function_Legendre_polynomials/Gl/*dat')
Gl_names.sort()
    
A_omega=np.zeros((len(A_omega_names),len(omega)))
Gl=np.zeros((len(Gl_names),80))
print(Gl.shape)
                
for i in range (len(A_omega_names)):
    A_omega[i]=np.loadtxt(A_omega_names[i])
    Gl[i]=np.loadtxt(Gl_names[i])    

# Normalisation between 0 and 1 for the sigmoid
A_max_per_column=A_omega.max(axis=1)
A_omega_norm=A_omega/A_max_per_column[:,None]
    
# Test and training set
training_size=int(np.ceil(Gl.shape[0]*4/5))
Gl_train=Gl[:training_size]
Gl_test=Gl[training_size:Gl.shape[0]]
A_omega_train=A_omega_norm[:training_size]
A_omega_test=A_omega_norm[training_size:Gl.shape[0]]
stddev=np.mean(abs(Gl[:,30]))
print(stddev)

#######################################################################################

if __name__ == "__main__":       
    
    start_time = time.time()
        
    nodes=np.array([100])
    batch_size=np.array([10])
    epochs=np.array([200])
    nodes_batch_epochs_list = list(itertools.product(nodes,batch_size,epochs))
    
    pool = Pool(processes=len(nodes_batch_epochs_list))
    pool.map(training,nodes_batch_epochs_list)
    
#    results.close()
    
    total_time = time.time() - start_time
    print('-'*50 + '\nDONE')
    print('Training took {:.0f} s.'.format(total_time))




