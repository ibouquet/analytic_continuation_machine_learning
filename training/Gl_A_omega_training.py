#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 10:57:32 2020

@author: ilanbouquet

Get A_omega, Gl and omega from the directory spectral_function_Legendre_polynomials
Train with the hyperparameters: nb_neurons, batch_size and epochs.
Model is saved under ./models/model_name 
"""


import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
import os, time
import itertools
from multiprocessing import Pool
import glob
import tensorflow as tf


def create_directory (path):
    dirname=str(path)
    if ~os.path.isdir(dirname):
        os.makedirs(dirname,exist_ok=True)
    return 

def training(nodes_batch_epochs_list):
    
    """ Training with the hyperparameters list and sigmoid activation function 
    Input = Gl and Output = A_omega with corresponding shape
    model_name='sigmoid' save the predictions in ../predictions/model_name/predictions.dat 
    omega and A_omega_test are saved in ../predictions/ """
    
    # Input Output shape
    input_shape=Gl.shape[1]
    output_shape=A_omega.shape[1]

    nodes=nodes_batch_epochs_list[0]
    batch=nodes_batch_epochs_list[1]
    epochs=nodes_batch_epochs_list[2]
            
    model = Sequential([
        Dense(nodes, activation='sigmoid', input_shape=(input_shape,)),
        Dense(nodes, activation='sigmoid'),
        Dense(nodes, activation='sigmoid'),
        Dense(nodes, activation='sigmoid'),
        Dense(output_shape, activation='sigmoid'),
        ])

    model.compile(optimizer='adam',loss='mean_squared_error',metrics=['MeanSquaredError'])

    model.fit(Gl_train,A_omega_train,epochs=epochs,batch_size=batch)

    model.save('./models/sigmoid')
    
    model_name='sigmoid'
    path_directory='../predictions/'+model_name
    
    predictions=model.predict(Gl_test)
    create_directory (path_directory)
    np.savetxt(path_directory+'/predictions.dat',predictions)
    
    np.savetxt('../predictions/A_omega_test.dat',A_omega_test)
    np.savetxt('../predictions/omega.out',omega)
######################################################################################
# Data collection#
 
omega=np.loadtxt('../spectral_function_Legendre_polynomials/omega.out')
A_omega_names=glob.glob('../spectral_function_Legendre_polynomials/A_omega/*dat')
A_omega_names.sort()
Gl_names=glob.glob('../spectral_function_Legendre_polynomials/Gl/*dat')
Gl_names.sort()
    
A_omega=np.zeros((len(A_omega_names),len(omega)))
Gl=np.zeros((len(Gl_names),80)) # length lmax
                
for i in range (len(A_omega_names)):
    A_omega[i]=np.loadtxt(A_omega_names[i])
    Gl[i]=np.loadtxt(Gl_names[i])    

# Normalisation between 0 and 1 for the sigmoid
A_max_per_column=A_omega.max(axis=1)
A_omega_norm=A_omega/A_max_per_column[:,None]
    
# Test and training set 4/5 for the training and 1/5 for the testing
training_size=int(np.ceil(Gl.shape[0]*4/5))
Gl_train=Gl[:training_size]
Gl_test=Gl[training_size:Gl.shape[0]]
A_omega_train=A_omega_norm[:training_size]
A_omega_test=A_omega_norm[training_size:Gl.shape[0]]

#######################################################################################

if __name__ == "__main__":       
    
    start_time = time.time()
        
    nodes=np.array([100])
    batch_size=np.array([10])
    epochs=np.array([200])
    nodes_batch_epochs_list = list(itertools.product(nodes,batch_size,epochs))
    
    pool = Pool(processes=len(nodes_batch_epochs_list))
    pool.map(training,nodes_batch_epochs_list)
    
    
    total_time = time.time() - start_time
    print('-'*50 + '\nDONE')
    print('Training took {:.0f} s.'.format(total_time))




