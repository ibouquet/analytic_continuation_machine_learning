"""
Created on Mon Jul 27 10:57:32 2020

Load the desired model and compute the spectral function predicted from the noisy Gl
Noise directories are in ../spectral_function_Legendre_polynomials/Gl_noise*

@author: ilanbouquet """

import numpy as np
import matplotlib.pyplot as plt
import glob,os
import tensorflow 
from tensorflow import keras


def create_directory (path):
    dirname=str(path)
    if ~os.path.isdir(dirname):
        os.makedirs(dirname,exist_ok=True)
    return 

            
def dat_files_into_np_array (filenames):
    
    """ Take the .dat files and concatenate them into np.array """
    
    filenames.sort()
    data_tmp=np.loadtxt(filenames[0])
    sx=len(filenames)
    sy=data_tmp.shape[0]
    np_array=np.zeros((sx,sy))
    for i in range (len(filenames)):
        np_array[i]=np.loadtxt(filenames[i])
    return np_array


######################################################################################
path_model='../training/models/sigmoid'
model_name=path_model.split('/')[-1]
lmax=80

A_omega_test=np.loadtxt('A_omega_test.dat')   
shape_test=A_omega_test.shape

noise_dir=glob.glob('../spectral_function_Legendre_polynomials/Gl_noise*')
Gl_noise=np.zeros((len(noise_dir),5000,lmax)) # 3D matrix with 1st dimension corresponding to the noise level

for names,i in zip(noise_dir,range (len(noise_dir))):
    
    create_directory(model_name+'/'+names.split('/')[-1]) #Create directory ./Gl_noise_xxx
    noise_dir[i]+='/*dat'
    Gl_noise_names=glob.glob(noise_dir[i]) # Collect all string names from Gl_noise_xxx
    Gl_noise[i]=dat_files_into_np_array (Gl_noise_names) # Convert into np.array
    
Gl_noise_test=Gl_noise[:,-shape_test[0]:5000]    

# Predictions 

predictions = np.zeros((len(noise_dir),shape_test[0],shape_test[1]))
print('start loading model')
model = tensorflow.keras.models.load_model(path_model)
print('model loaded')

for i in range(predictions.shape[0]): # Save predictions for each noise level in the right directory
    save_path=model_name+'/'+noise_dir[i].split('/')[-2]
    predictions[i]=model.predict(Gl_noise_test[i])
    np.savetxt(save_path+'/'+'predictions.dat',predictions[i],fmt='%1.5f')


    