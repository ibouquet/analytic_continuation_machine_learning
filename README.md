# Analytic Continuation with Machine Learning
Written by I.Bouquet from Materials Theory at ETH Zurich.


This program is structured in three parts:
1. Spectral function to Legendre polynomials 
    - Generation of spectral functions
    - Conversion of the spectral function to Gl coefficients
2. Training
    - Training on two models: sigmoid and sigmoid_robust
    - Write the predictions on Gl_test in the predictions directory
3. Predictions
    - Contains the results from the models
    - Script to load a saved model and test it on test set (i.e. noisy_Gl)

## Spectral function to Legendre polynomials
To be run in docker container:triqs_vasp 

`'docker run --rm -it --shm-size=4g -e USER_ID=197835 -e GROUP_ID=1029 -p 8378:8378 -v /home/username:/nas triqs_vasp bash'`

To generate n A_omega with corresponding n Gl coefficients 


`run_A_omega_and_Gl.py 5000 beta lmax noise`

For example for 5000 repetions with -1<omega<1 with lmax=80 and noise 

`run_A_omega_and_Gl.py n_repetitions 200 80 1`

Directories A_omega, Gl, Gl_noise are created containing the corresponding .dat files plus the omega.out vector.

## Training

To be run in docker container: tensorflow/tensorflow


`docker pull tensorflow/tensorflow`


`docker run --rm -it -u 197835:1029 -p 8378:8378 -v /home/username:/nas tensorflow/tensorflow bash`


run Training on sigmoid


`python Gl_A_omega_training.py`

Will read the A_omega and Gl from spectral_function_to_Legendre_polynomials directory and write the results in the predictions directory in the coreesponding model directory.
Model parameters are saved in the current directory under models. 

## Predictions

Directories with model name containing the predictions.

If noisy Gl generated use 


` python predictions_noisy_Gl.py` and update `path_model='../training/models/model_name'` 


Create a directory with noise level and write the predictions with the right model.

